
export default {
  en: {
    about: "About",
    back: "Back",
    home: "Home",
    toggle_dark: "Toggle dark mode",
    toggle_langs: "Change languages",
    desc: "Fastest way to find drugs prices",
    search: "Search by name",
    updated: "updated",
    not_found: "Not found"
  },
  fr: {
    about: "À propos",
    back: "Retour",
    home: "Accueil",
    toggle_dark: "Mode sombre",
    toggle_langs: "Changer de langue",
    desc: "Le moyen le plus rapide de trouver les prix des médicaments",
    search: "Recherche ultra-rapide",
    updated: "mis à jour",
    not_found: "Non trouvé"
  },
  ru: {
    about: "О нас",
    back: "Назад",
    home: "Главная",
    toggle_dark: "Переключить темный режим",
    toggle_langs: "Сменить язык",
    desc: "Самый быстрый способ найти цены на лекарства",
    search: "Поиск по названию / Коду или МНН",
    updated: "обновлено",
    not_found: "Не найдено"
  }
}
