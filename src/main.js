import { createApp } from 'vue'
import router from './router'
import locales from '../locales/locales'
import {createI18n} from 'vue-i18n'
import './style.css'
import App from './App.vue'
import OpenLayersMap from 'vue3-openlayers'
import "vue3-openlayers/styles.css"; // vue3-openlayers version < 1.0.0-*
import "vue3-openlayers/dist/vue3-openlayers.css"; // vue3-openlayers version >= 1.0.0-*

const app = createApp(App)
const i18n = createI18n({
  legacy: false,
  locale: 'fr',
  messages: locales,
})

app.use(router)
app.use(i18n)
app.use(OpenLayersMap)
app.mount('#app')
