import {createRouter, createWebHistory} from 'vue-router'
import DrugSearch from './pages/DrugSearch.vue'
import Cart from './pages/Cart.vue'
import Pharmacies from './pages/Pharmacies.vue'
import Pharmus from './pages/Pharmus.vue'


const routes = [
  {
    path: '/',
    component: DrugSearch,
  },
  {
    path: '/cart',
    component: Cart,
  },
  {
    path: '/pharmacies',
    component: Pharmacies,
  },
  {
    path: '/pharmus',
    component: Pharmus,
  },
]

const router = createRouter({ history: createWebHistory(), routes})

export default router
