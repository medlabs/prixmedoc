# Prixmedoc Project Roadmap

### Not Started
- Add Cart
- Add Vidal Content

### In Progress
- Map of pharmacies
- Tooltip on pharmacies points on map showing infos.
- add Help to Pharmus
- add Molecules words with different length

### Completed
- Drugs fast search
- Python script to scrape data
- Dark mode
