import requests
from bs4 import BeautifulSoup
import pandas as pd
import json
import urllib.parse
import re
import shutil
url = 'https://spot.tn/?mayor=articles&id=337'
response = requests.get(url)

soup = BeautifulSoup(response.content, 'html.parser')
download_link = soup.select_one('a[href$=".xls"]')['href']
download_link = urllib.parse.urljoin(url, download_link)
download_link = urllib.parse.quote(download_link, safe=':/')

# Get the Date from the file name
date_match = re.search(r'\d{2}-\d{2}', download_link)
if date_match:
    date = date_match.group()
else:
    date = 'unknown'

temp_file, _ = urllib.request.urlretrieve(download_link)


print('Downloading file from : ', temp_file)
print('File Date: ', date)

df = pd.read_excel(temp_file)

data = []
for _, row in df.iterrows():
    data.append(row.to_dict())

output_data = {
    "updated": date,
    "data": data
}

with open(f'./scraped/output_{date}.json', 'w') as f:
    json.dump(output_data, f)

# Copying the file to public folder
shutil.copy(f'./scraped/output_{date}.json', '../public/data/output.json')
