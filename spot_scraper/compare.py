import json

# Read the content of the OLD output.json
with open('../public/data/output.json', 'r') as f:
    old_data = json.load(f)

# Read the content of the new output file
with open('./scraped/output_08-06.json', 'r') as f:
    new_data = json.load(f)

    # Compare the dictionaries and replace the differences in the OLD dictionary
for key, new_val in new_data.items():
    old_val = old_data.get(key)
    if old_val != new_val:
        old_data[key] = new_val

        # Write the updated dictionary back to the output.json file
with open('../public/data/output.json', 'w') as f:
    json.dump(old_data, f)
